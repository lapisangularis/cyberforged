+++
title = "About"
date = "2020-04-02"
aliases = ["about-me","about-lapis","contact"]
[ author ]
  name = "lapisangularis"
+++

In the web they call me **lapisangularis**, and I'm a **Software Enginner**. I'm doing some things over the internet. 

In real, my name is **Przemek Nowakowski**, living a comfy life in **Poland**. My job is mainly Software Engineering. I'm doing it for several years now, and I did it from development, through devops to project management and mentoring. I'm a self-employed Mercenary Engineer, you could say - in a one person company called **[cyberforged](https://cyberforged.io/)**. I'm a coder, hacker, wannabe scientist. I like keeping things simple.

As **[cyberforged](https://cyberforged.io/)**, I'm offering you **Software Craftsmanship Services**. If you have a project for development or technical management, feel free to contact me. I'll get in touch with you, and either send you the **free project quota**, or recommend some other **software development company**. Well, I'm just one person, I'm not always free for another project. And not all projects are suited for one developer.

If you're a technical person (or not!) I highly recommend you visiting my **personal webpage**, where you can find contact information for non-job related topics. Also there are some (mostly engineering stuff) **articles and blog posts**. Thank you and welcome!
