+++
title = "Portfolio"
date = "2020-04-02"
aliases = ["portfolio","cv","job-history"]
[ author ]
  name = "Przemysław Nowakowski"
+++

## Experience

I'm not some no-name engineer without any real experience. People and companies who have worked with me were always satisfied with my professional approach. I have a lot of experience, either commercial, opensource, or just... My own. See below if you don't believe! :)

#### [bPol](https://bpol.net/) - Software Engineer
*04.2020 - currently, full-time b2b contractor*

* Developing new backend solutions for business projects
* Refactoring, bugfixing and legacy code maintanance
* Introducing new technologies to projects, cooperation with teammates, mentoring

#### [Unicornly](https://unicornly.io/) - Software Engineer
*04.2019 - 04.2020, full-time b2b contractor*

* Developing and designing new backend PHP and Python applications
* Helping with creating frontend solutions in React
* Refactoring and helping with existing projects codebase
* Introducing new technologies to projects, mentoring for junior developers

#### [The Software House](https://tsh.io/) - Software Engineer
*09.2017 - 04.2019, full-time b2b contractor*

* Developing new backend solutions with PHP for business projects
* Refactoring, bugfixing and legacy code maintanance
* Introducing new technologies to projects, cooperation with teammates

#### [Impedio Security](https://impedio-security.com/) - Software Engineer
*01.2017 - 03.2018, part-time b2b contractor*

* Lead development for Ransomware Shield web application
* Servers Administration, quick reacting for incidents
* Designing and advising with application system architecture

#### [Landingi](https://landingi.com/) - Software Developer
*10.2016 - 09.2017, full-time b2b contractor*

* Development of new features for landingi dashboard and backend, improving landing pages editor in JS
* Quick reacting for imminent bugs, code refactoring, improving and writing unit tests and specs
* Preparing legacy code for migration to PHP7 and new infrastructure.

#### [Skygate](https://skygate.io/) - DevOps Engineer & Software Developer
*04.2016 - 10.2016, internship*

* Taking care of development machine, office network, production servers and instances.
* Developing new CI processes and pipelines in projects, quick reacting for major failures.
* Backend Development in PHP7, preparing REST API for frontend, in multiple internal projects and for clients.

## Join the list

You feel convinced? If not, go for Services section for more info how can I help you. If yes - just send me an email, and I'll write you back!
