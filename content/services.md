+++
title = "Services"
date = "2020-04-02"
aliases = ["services"]
[ author ]
  name = "Przemysław Nowakowski"
+++

## Mercenary for hire

Like in the wild west - provide me resources, and a mercenary will do anything to achieve your goals. But remember, we're talking about Software Engineering area only. ;)

{{< box-container >}}
{{< box-child title="Full-time Engineer" img="/img/cross-red.png" >}}
<center><i style="color: darkred">currently not available</i></center><br>
As a mercenary, I can work with a full-time B2B contract. Software Engineering, DevOps, Architect services included.
{{< /box-child >}}
{{< box-child title="Part-time Engineer" img="/img/check-green.png" >}}
<center><i style="color: darkgreen">available</i></center><br>
With limited weekly time and availability of up to 20 hours, I'm open for part-time Software Craftsmanship and DevOps projects.
{{< /box-child >}}
{{< box-child title="Expertise & Consulting" img="/img/check-green.png" >}}
<center><i style="color: darkgreen">available</i></center><br>
You want to consult, seek a tech schooling or an advice in your project or idea? You have probably found a good person.
{{< /box-child >}}
{{< /box-container >}}

You can send me an email, if you want to talk about details! The first rough project quota is a free bonus. ;)

## Trusted technologies

I develop software using the best and most cutting edge technologies. The priority is always to use a proper tool for specific project.

{{< box-container >}}
{{< box-child title="PHP" img="/img/php.png" >}}
Using <b>Symfony</b> and <b>Doctrine</b> I can develop cutting edge apps - PHP is not dead!
{{< /box-child >}}
{{< box-child title="Python" img="/img/python.png" >}}
Fast and simple development for web apps with <b>Django</b> or data science with <b>TensorFlow</b>.
{{< /box-child >}}
{{< box-child title="JS Frontend" img="/img/frontend.webp" >}}
Frontend for typical backend dev is always hard. But with <b>VueJS</b> nothing is impossible.
{{< /box-child >}}
{{< box-child title="Node.js" img="/img/node.png" >}}
<b>JavaScript</b> on backend? Some say, it's blasphemy. Others - just a project requirement.
{{< /box-child >}}
{{< box-child title="Golang" img="/img/go.svg" >}}
<b>Google</b>'s lovely child can do amazing things when used for right purposes. Wanna try?
{{< /box-child >}}
{{< box-child title="AWS" img="/img/aws.png" >}}
The must have <b>IaaS</b> for every startup. <b>Devops</b> love it. Developers love it. And You will too.
{{< /box-child >}}
{{< box-child title="Docker" img="/img/docker.webp" >}}
The greatest <b>containerization</b> tool amongst all. Unified development and production environment. Easy <b>deployment</b>.
{{< /box-child >}}
{{< box-child title="Databases" img="/img/db.jpg" >}}
Almost no app can exist without it. I'm using <b>PostgreSQL</b>, MySQL, <b>MongoDB</b>, Redis and more - anything to achieve Your goal.
{{< /box-child >}}
{{< box-child title="Linux" img="/img/linux.png" >}}
Last but not least! Of course I love <b>Linux</b>! The first choice for client's application environment, or build <b>server</b>.
{{< /box-child >}}
{{< /box-container >}}

## But there's more...

Of course it is not the full list of skills and technologies I know! Almost all of them, which are worthy mentioning are contained on the list below! Maybe something will catch your eye?

* **Languages** - PHP, Python, Node.js, JavaScript, TypeScript, Golang, Elixir, HTML, CSS, Bash
* **Server OS** - Linux (Debian, Ubuntu, Archlinux, Fedora, RedHat)
* **Web Frameworks** - Symfony, Laravel, Django, Express.js, NestJS, Vue.js, React.js, Gin, Phoenix, Bootstrap, Semantic UI, Hugo
* **Other Frameworks** - Doctrine, Eloquent, GORM, Sequelize, TypeORM, Ecto, TensorFlow, Keras, socket.io, gRPC, ElasticSearch
* **Testing tools** - PHPUnit, PHPSpec, Behat, Robohydra, Ginkgo, PyTest, ExUnit, Mocha, Jest, Protractor, Selenium
* **DevOps tools** - AWS, Docker, Kubernetes, Ansible, Vagrant, LXC, Jenkins, Google Cloud, Scaleway, GitlabCI, TravisCI, Bitbucket Pipelines, Nginx, Logstash, Sentry, Nagios, Grafana, Terraform, CloudFlare
* **Other tools** - Git, RabbitMQ, AWS SQS, jQuery, Twig, Redux
* **Databases** - MySQL, PostgreSQL, MongoDB, Redis, AWS DynamoDB
* **Architecture** - REST, CQRS, DDD, TDD, BDD, Hexagonal/Clean Architecture, Backend For Frontend (BFF), Microservices
* **Methodologies** - Scrum, Kanban, Agile

You don't see your tool or tech, that maybe You want to use in your project? Well, I'm a fast learner, and very open for new stuff! Contact me if you want to learn more!