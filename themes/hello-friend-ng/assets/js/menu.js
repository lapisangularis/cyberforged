// Mobile menu

const menuTrigger = document.querySelector(".menu-trigger");
const menuTriggerParent = document.querySelector(".menu-parent");
const subMenu = document.querySelector(".sub-menu");
const menu = document.querySelector(".menu");
const mobileQuery = getComputedStyle(document.body).getPropertyValue("--phoneWidth");
const isMobile = () => window.matchMedia(mobileQuery).matches;
const isMobileMenu = () => {
  menuTrigger && menuTrigger.classList.toggle("hidden", !isMobile());
  menu && menu.classList.toggle("hidden", isMobile());
};
const isSubMenu = () => {
  subMenu && subMenu.classList.toggle("hidden");
};

isMobileMenu();
isSubMenu();

menuTrigger && menuTrigger.addEventListener("click", () => menu && menu.classList.toggle("hidden"));
menuTriggerParent && menuTriggerParent.addEventListener("click", () => subMenu && subMenu.classList.toggle("hidden"));

window.addEventListener("resize", isMobileMenu);
window.addEventListener("resize", isSubMenu);
